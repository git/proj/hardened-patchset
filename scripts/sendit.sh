#!/bin/bash

REMOTE=blueness@dev.gentoo.org
RDIR=/home/blueness/public_html/hardened-sources/hardened-patches

scp $1 ${REMOTE}:${RDIR}
