#!/bin/bash

SCRIPT_NAME="${0}"
KERNEL_VER="${1%/}"
HGPV_PATCH="${2}"
TAR="/bin/tar"
GPG="/usr/bin/gpg"
SIGNAS="F52D4BBA"

################################################################################

die() {
	echo "$1"
	exit
}

sanity() {
	[[ -d "${KERNEL_VER}" ]]	|| die "Can't find ${KERNEL_VER} directory"
	[[ -e "${TAR}" ]]		|| die "Can't find tar"
	[[ -n "${HGPV_PATCH}" ]]	|| die "No HGPV patchlevel given"
}

################################################################################

sanity

HGPV="${KERNEL_VER}-${HGPV_PATCH}"
HGPV_TARBALL="hardened-patches-${HGPV}.extras.tar.bz2"

$TAR jcvf ${HGPV_TARBALL} ${KERNEL_VER}

$GPG -a -s -b --default-key $SIGNAS ${HGPV_TARBALL}
